
# GameDevSession : I N F E C T I O N

## Concept

Standing shooter en Réalité Virtuelle.
Malade, notre but est d'envoyer mouchoir ou autre source infectueuse pour contaminer une cible, qui ira ensuite contaminer d'autres personnes.
Le but est de contaminer le plus de personnes.

### Environnement 

Il sera basé sur des situations de la vie quotidienne, du type : Salle de classe, bureau de travail, bar, parc...

### Difficulté

La difficulté sera gérée en fonction du niveau d'accessiblité des cibles et de leur résistance (ex. : personnes agées et enfants facilement contaminables mais peu endurants... ).


## Inspirations

* Plague Inc.
* Burnout 3 (Crash mode)
* Paint the town red


## Ressources 

*  [Unity VR](https://unity3d.com/fr/learn/tutorials/topics/xr/getting-started-vr-development)
*  [Realiteer](http://www.realiteer.com/diy)
*  